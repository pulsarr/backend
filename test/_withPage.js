const puppeteer = require('puppeteer');

/**
 * @param {any} t test to be run
 * @param {Function} run ava test runner
 * @async
 * @private
 */
async function withPage(t, run) {
  const browser = await puppeteer.launch({
    headless: true,
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-dev-shm-usage',
    ],
  });

  const page = await browser.newPage();

  try {
    await run(t, page);
  } finally {
    await page.close();
    await browser.close();
  }
}

module.exports = withPage;
