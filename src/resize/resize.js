const fs = require('fs').promises;
const path = require('path');
const sharp = require('sharp');
const { list } = require('./disk');

/**
 * @param {object} arguments function arguments
 * @param {string} arguments.sourceFile source file
 * @param {string} arguments.destination destination folder
 * @param {number} arguments.width target width
 * @param {number} arguments.height target height
 * @param {boolean} arguments.splitLandscape should split landscape images?
 * @param {boolean} arguments.prependLandscape should include original image before split?
 * @param {boolean} arguments.leftFirst should left be first image?
 */
async function resizeImage({
  sourceFile,
  destination,
  width = 1404,
  height = 1872,
  splitLandscape = true,
  prependLandscape = true,
  leftFirst = true,
}) {
  const image = sharp(sourceFile);
  const metadata = await image.metadata();

  await fs.mkdir(destination, { recursive: true });

  const imageName = path.basename(sourceFile);
  const imageNoExtension = path.parse(imageName).name;

  const resizeOptions = {
    width,
    height,
    fit: 'inside',
  };

  if (splitLandscape && metadata.width > metadata.height) {
    if (prependLandscape) {
      await image
        .png()
        .resize(resizeOptions)
        .toFile(path.join(destination, `${imageNoExtension}-0.png`));
    }

    let leftFileName = `${imageNoExtension}-2.png`;
    let rightFileName = `${imageNoExtension}-1.png`;

    if (leftFirst) {
      leftFileName = `${imageNoExtension}-1.png`;
      rightFileName = `${imageNoExtension}-2.png`;
    }

    // left
    await sharp(sourceFile)
      .extract({
        left: 0,
        top: 0,
        width: metadata.width / 2,
        height: metadata.height,
      })
      .png()
      .resize(resizeOptions)
      .toFile(path.join(destination, leftFileName));

    // right
    await sharp(sourceFile)
      .extract({
        left: metadata.width / 2,
        top: 0,
        width: metadata.width / 2,
        height: metadata.height,
      })
      .png()
      .resize(resizeOptions)
      .toFile(path.join(destination, rightFileName));
  } else {
    await await image
      .png()
      .resize(resizeOptions)
      .toFile(path.join(destination, `${imageNoExtension}.png`));
  }
}

/**
 * @param {object} arguments function arguments
 * @param {string} arguments.source source directory
 * @param {string} arguments.destination destination directory
 * @param {number} [arguments.width=1404] target width
 * @param {number} [arguments.height=1872] target height
 * @param {boolean} [arguments.splitLandscape=true] split landscape images into two?
 * @param {boolean} [arguments.prependLandscape=true] include original image before split images?
 * @param {boolean} [arguments.leftFirst=true] once split left image comes before right image
 * @returns {Promise<void>}
 */
async function resize({
  source,
  destination,
  width = 1404,
  height = 1872,
  splitLandscape = true,
  prependLandscape = true,
  leftFirst = true,
}) {
  const sourceFiles = await list(source);
  return Promise.all(
    sourceFiles.map((s) => {
      const relativePath = path.dirname(s.file.replace(source, ''));

      return resizeImage({
        sourceFile: s.file,
        destination: path.join(destination, relativePath),
        width,
        height,
        splitLandscape,
        prependLandscape,
        leftFirst,
      });
    })
  );
}

module.exports = {
  resize,
  resizeImage,
};
