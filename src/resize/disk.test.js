const test = require('ava');
const path = require('path');
const { list } = require('./disk');

const source = path.join(__dirname, '../../test/images');

test('list files happy path', async (t) => {
  const expected = ['/portrait.png', '/landscape.png'];

  const sourceFiles = await list(source);
  const relativePaths = sourceFiles.map((d) => d.file.replace(source, ''));

  t.deepEqual(relativePaths.sort(), expected.sort());
});
