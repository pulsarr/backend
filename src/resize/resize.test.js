const test = require('ava');
const path = require('path');
const tmp = require('tmp');
const { resize, resizeImage } = require('./resize');
const { list } = require('./disk');

tmp.setGracefulCleanup();

const source = path.join(__dirname, '../../test/images');
const portrait = path.join(source, 'portrait.png');
const landscape = path.join(source, 'landscape.png');

test('resize happy path', async (t) => {
  const expected = [
    '/portrait.png',
    '/landscape-0.png',
    '/landscape-1.png',
    '/landscape-2.png',
  ];

  const destination = tmp.dirSync().name;

  await resize({
    source,
    destination,
  });

  const destinationFiles = await list(destination);
  const relativePaths = destinationFiles.map((d) =>
    d.file.replace(destination, '')
  );

  t.deepEqual(relativePaths.sort(), expected.sort());
});

test('resize portrait happy path', async (t) => {
  const expected = ['/portrait.png'];

  const destination = tmp.dirSync().name;

  await resizeImage({
    sourceFile: portrait,
    destination,
  });

  const destinationFiles = await list(destination);
  const relativePaths = destinationFiles.map((d) =>
    d.file.replace(destination, '')
  );

  t.deepEqual(relativePaths.sort(), expected.sort());
});

test('resize landscape happy path', async (t) => {
  const expected = ['/landscape-0.png', '/landscape-1.png', '/landscape-2.png'];

  const destination = tmp.dirSync().name;

  await resizeImage({
    sourceFile: landscape,
    destination,
  });

  const destinationFiles = await list(destination);
  const relativePaths = destinationFiles.map((d) =>
    d.file.replace(destination, '')
  );

  t.deepEqual(relativePaths.sort(), expected.sort());
});

test('resize landscape no prepend', async (t) => {
  const expected = ['/landscape-1.png', '/landscape-2.png'];

  const destination = tmp.dirSync().name;

  await resizeImage({
    sourceFile: landscape,
    destination,
    prependLandscape: false,
  });

  const destinationFiles = await list(destination);
  const relativePaths = destinationFiles.map((d) =>
    d.file.replace(destination, '')
  );

  t.deepEqual(relativePaths.sort(), expected.sort());
});

test('resize landscape no split', async (t) => {
  const expected = ['/landscape.png'];

  const destination = tmp.dirSync().name;

  await resizeImage({
    sourceFile: landscape,
    destination,
    splitLandscape: false,
  });

  const destinationFiles = await list(destination);
  const relativePaths = destinationFiles.map((d) =>
    d.file.replace(destination, '')
  );

  t.deepEqual(relativePaths.sort(), expected.sort());
});
