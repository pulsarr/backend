const fs = require('fs').promises;
const path = require('path');

/**
 * @typedef FileAndInfo
 * @property {string} file the full path of the file
 * @property {boolean} isDir true if a directory false otherwise
 */

/**
 * @async
 * @param {string} directory root directory to start walking
 * @returns {Promise<Array<FileAndInfo>>} an array of all directories and files
 */
async function list(directory) {
  let files = await fs.readdir(directory);
  files = await Promise.all(
    files.map(async (file) => {
      const absolutePath = path.join(directory, file);
      const stats = await fs.stat(absolutePath);

      const results = [
        {
          file: absolutePath,
          isDir: stats.isDirectory(),
        },
      ];

      if (stats.isDirectory()) {
        const directoryFiles = await list(absolutePath);
        return [...results, ...directoryFiles];
      }

      return results;
    })
  );

  return files.flat();
}

module.exports = {
  list,
};
