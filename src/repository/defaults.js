const page = {
  pageSize: 10,
  page: 0,
};

module.exports = {
  all: { ...page },
  find: { ...page },
};
