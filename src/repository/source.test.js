const test = require('ava');
const { all, save } = require('./source');

test('int source should be able to save a new source', async (t) => {
  const expected = {
    name: `TestMangaSource-${Math.random()}`,
    url: 'https://www.example.com',
  };

  const s = await save(expected);

  t.truthy(s.id);
  t.is(s.name, expected.name);
  t.is(s.url, expected.url);
});

test('int source should be able to upsert one field', async (t) => {
  const expected = {
    name: `TestMangaSource-${Math.random()}`,
    url: 'https://www.example.com',
  };

  let s = await save(expected);

  t.truthy(s.id);
  t.is(s.name, expected.name);
  t.is(s.url, expected.url);

  expected.url = 'https://example.com';

  s = await save(expected);

  t.truthy(s.id);
  t.is(s.name, expected.name);
  t.is(s.url, expected.url);
});

test('int source should be able to list all', async (t) => {
  const expected = {
    name: `TestMangaSource-${Math.random()}`,
    url: 'https://www.example.com',
  };

  await save(expected);

  const sources = await all();

  t.true(sources.length > 0);
});

test('int source should be able find by id', async (t) => {
  const expected = {
    name: `TestMangaSource-${Math.random()}`,
    url: 'https://www.example.com',
  };

  const s = await save(expected);

  const sources = await all({
    id: s.id,
  });

  t.true(sources.length > 0);
  t.deepEqual(s, sources[0]);
});

test('int source should be able find by name', async (t) => {
  const expected = {
    name: `TestMangaSource-${Math.random()}`,
    url: 'https://www.example.com',
  };

  const s = await save(expected);

  const sources = await all({
    name: s.name,
  });

  t.true(sources.length > 0);
  t.deepEqual(s, sources[0]);
});

test('int source should be able find by url', async (t) => {
  const expected = {
    name: `TestMangaSource-${Math.random()}`,
    url: `https://www.example.com-${Math.random()}`,
  };

  const s = await save(expected);

  const sources = await all({
    url: s.url,
  });

  t.true(sources.length > 0);
  t.deepEqual(s, sources[0]);
});
