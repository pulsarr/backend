const { Pool } = require('pg');

const pool = new Pool();

/**
 * @async
 * @param {string} text Select * from....$1
 * @param {Array} parameters Parameters populated in query
 * @returns {Promise} returns executed pool query
 */
async function query(text, parameters) {
  return pool.query(text, parameters);
}

module.exports = {
  query,
};
