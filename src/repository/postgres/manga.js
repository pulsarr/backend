const { query } = require('./pool');

/**
 * @async
 * @param  {object} options query options
 * @param  {number} [options.pageSize=10] Sets page size limit
 * @param  {number} [options.page=0] Returns a different page
 * @returns {Promise} array of all sources within limit
 */
async function all(options) {
  const defaults = {
    pageSize: 10,
    page: 0,
  };
  const updatedOptions = { ...defaults, ...options };
  const offset = updatedOptions.pageSize * updatedOptions.page;

  try {
    const result = await query(
      `SELECT * from "public"."source" LIMIT ${updatedOptions.pageSize} OFFSET ${offset};`
    );
    return result.rows;
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

/**
 * @async
 * @param {object} source new source to be inserted
 * @param {string} source.name The name of the Source 'Manga Reader'
 * @param {string} source.url The url to the source
 * @returns {Promise} returns inserted source
 */
async function insert(source) {
  try {
    //
    const result = await query(
      'INSERT INTO "public"."source" ("name", "url") VALUES ($1, $2) RETURNING *;',
      [source.name, source.url]
    );
    return result.rows[0] || result.rows;
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

/**
 * @param {object} source to be updated
 * @param {number} source.id The id of the source
 * @param {string} source.name The name of the Source 'Manga Reader'
 * @param {string} source.url The url to the source
 * @returns {Promise} updated source
 */
async function update(source) {
  const u = Object.keys(source)
    .filter((k) => k !== 'id')
    .map((k) => `"${k}" = '${source[k]}'`);

  if (u.length === 0) {
    return Promise.reject(new Error('nothing to update'));
  }

  try {
    const result = await query(
      `UPDATE "public"."source" SET ${u.join(
        ','
      )} WHERE "id" = $1 RETURNING *;`,
      [source.id]
    );
    return result.rows[0] || result.rows;
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

module.exports = {
  all,
  insert,
  update,
};
