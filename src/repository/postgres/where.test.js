const test = require('ava');
const { where } = require('./where');

const testCases = [
  {
    message: 'where with a single valid key',
    expected: {
      query: 'WHERE "name" = $1',
      args: ['name'],
    },
    keys: ['name'],
    all: {
      id: 1,
      name: 'name',
    },
  },
  {
    message: 'no keys',
    expected: {
      query: '',
      args: [],
    },
    keys: [],
    all: {
      id: 1,
      name: 'name',
    },
  },
  {
    message: 'no valid keys',
    expected: {
      query: '',
      args: [],
    },
    keys: ['cat'],
    all: {
      id: 1,
      name: 'name',
    },
  },
  {
    message: 'multiple valid keys',
    expected: {
      query: 'WHERE "name" = $1 AND "url" = $2',
      args: ['name', 'url'],
    },
    keys: ['name', 'url'],
    all: {
      id: 1,
      name: 'name',
      url: 'url',
    },
  },
  {
    message: 'undefined value',
    expected: {
      query: '',
      args: [],
    },
    keys: ['name'],
    all: {
      id: 1,
      name: undefined,
    },
  },
];

test('where clause', (t) => {
  testCases.forEach((testCase) => {
    const w = where(testCase.keys, testCase.all);
    t.deepEqual(w, testCase.expected, testCase.message);
    t.is(w.query.split('$').length - 1 || 0, w.args.length, testCase.message);
  });
});
