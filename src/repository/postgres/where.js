/**
 * @typedef {object} WhereQuery
 * @property {string} query where clause 'WHERE x = ?'
 * @property {Array} args to be passed to query function
 */

/**
 * Creates a where clause for sql
 *
 * @param {Array} keys to use as a filter against the object all
 * @param {object} all the object with potentially any, all, none of the keys passed
 * @returns {WhereQuery} either an empty string if no keys match, or a proper where clause with AND
 */
function where(keys, all) {
  const queryArguments = [];
  const list = Object.keys(all)
    .filter((k) => keys.includes(k))
    .filter((k) => all[k] !== undefined)
    .map((k, i) => {
      queryArguments.push(all[k]);
      return `"${k}" = $${i + 1}`;
    });

  if (list.length > 0) {
    return {
      query: `WHERE ${list.join(' AND ')}`,
      args: queryArguments,
    };
  }

  return {
    query: '',
    args: [],
  };
}

module.exports = {
  where,
};
