const test = require('ava');
const source = require('./source');

test('int source should be able to insert', async (t) => {
  const expected = {
    name: 'name',
    url: 'https://www.example.com',
  };

  const s = await source.insert(expected);

  t.truthy(s.id);
  t.is(s.name, expected.name);
  t.is(s.url, expected.url);
});

test('int source should be able to update just name', async (t) => {
  const expected = {
    name: 'name',
    url: 'https://www.example.com',
  };

  const s = await source.insert(expected);

  t.truthy(s.id);
  t.is(s.name, expected.name);
  t.is(s.url, expected.url);

  const newName = 'newName';
  const updatedSourceName = await source.update({
    id: s.id,
    name: newName,
  });

  t.is(updatedSourceName.id, s.id);
  t.is(updatedSourceName.name, newName);
  t.is(s.url, expected.url);
});
