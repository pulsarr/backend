const { query } = require('./pool');

const queries = {
  createSourceTable: `
  CREATE TABLE "public"."source" (
    "id" serial,
    "name" text NOT NULL DEFAULT '',
    "url" text NOT NULL DEFAULT '',
    PRIMARY KEY ("id")
  );`,
  createMangaTable: '',
};

(async () => {
  await query(queries.createSourceTable);
})();
