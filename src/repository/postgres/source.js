const { query } = require('./pool');
const { where } = require('./where');

const selectOptionDefaults = {
  offset: 0,
};

/**
 * @async
 * @param  {object} [options={}] query options
 * @param  {number} [options.limit=10] Sets page size limit
 * @param  {number} [options.offset=0] Returns a different page
 * @param {number} options.id search by name
 * @param {string} options.name search by name
 * @param {string} options.url search by name
 * @returns {Promise<Array>} array of all sources within limit
 */
async function select(options) {
  const o = { ...selectOptionDefaults, ...options };
  const { query: w, args } = where(['id', 'name', 'url'], options);

  try {
    const result = await query(
      `SELECT * from "public"."source" ${w} LIMIT ${o.limit} OFFSET ${o.offset};`,
      args
    );
    return result.rows;
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

/**
 * @async
 * @param {object} source new source to be inserted
 * @param {string} source.name The name of the Source 'Manga Reader'
 * @param {string} source.url The url to the source
 * @returns {Promise<object>} returns inserted source
 */
async function insert(source) {
  try {
    //
    const result = await query(
      'INSERT INTO "public"."source" ("name", "url") VALUES ($1, $2) RETURNING *;',
      [source.name, source.url]
    );
    return result.rows[0] || result.rows;
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

/**
 * @param {object} source to be updated
 * @param {number} source.id The id of the source
 * @param {string} source.name The name of the Source 'Manga Reader'
 * @param {string} source.url The url to the source
 * @returns {Promise<object>} updated source
 */
async function update(source) {
  const u = Object.keys(source)
    .filter((k) => k !== 'id')
    .map((k) => `"${k}" = '${source[k]}'`);

  if (u.length === 0) {
    return Promise.reject(new Error('nothing to update'));
  }

  try {
    const result = await query(
      `UPDATE "public"."source" SET ${u.join(
        ','
      )} WHERE "id" = $1 RETURNING *;`,
      [source.id]
    );
    return result.rows[0] || result.rows;
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

module.exports = {
  select,
  insert,
  update,
};
