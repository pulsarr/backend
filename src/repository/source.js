const database = require('./postgres');

const allOptionsDefault = {
  pageSize: 10,
  page: 0,
};

/**
 * returns all and filters by id, name or url
 *
 * @async
 * @param {object} [options={}] query options
 * @param {number} [options.pageSize=10] max results per query
 * @param {number} [options.page=0] page index
 * @param {number} [options.id] source id
 * @param {string} [options.name] source name
 * @param {string} [options.url] source url
 * @returns {Promise} all sources
 */
async function all(options) {
  const o = { ...allOptionsDefault, ...options };
  o.limit = o.pageSize;
  o.offset = o.page * o.pageSize;

  return database.source.select(o);
}

/**
 * @async
 * @param {object} source new or existing source to be saved or updated
 * @param {number} [source.id] database id
 * @param {string} source.name unique source name
 * @param {string} source.url source url
 * @returns {Promise<object>} upserted source
 */
async function save(source) {
  const foundSource = await database.source.select({
    limit: 1,
    id: source.id,
    name: source.name,
  });

  if (foundSource.length > 0) {
    return database.source.update({ ...foundSource[0], ...source });
  }

  return database.source.insert(source);
}
module.exports = {
  all,
  save,
};
