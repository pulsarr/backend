const express = require('express');
const bodyParser = require('body-parser');
const repository = require('./repository');

const app = express();
const port = 3000;

app.use(bodyParser.json());

app.post('/source', async (request, response) => {
  try {
    const sources = await repository.source.all(request.body);
    response.json(sources);
  } catch {
    response.json({
      error: 'unable to query all sources',
    });
  }
});

// app.get('/source/new', async (request, response) => {
//   try {
//     const sources = await repository.source.insert({
//       name: 'MangaReader',
//       url: 'https://mangareader.com',
//     });
//     response.json(sources);
//   } catch {
//     response.json({
//       error: 'unable to query all sources',
//     });
//   }
// });

const server = app.listen(port, () =>
  console.log(`Example app listening at http://localhost:${port}`)
);

process.on('SIGTERM', () => {
  server.close(() => {
    console.log('Process terminated');
  });
});
