const test = require('ava');
const tmp = require('tmp');
const fs = require('fs').promises;
const path = require('path');
const { create } = require('./archive');

tmp.setGracefulCleanup();
const source = path.join(__dirname, '../../test/images');

test('create happy path', async (t) => {
  const destination = tmp.dirSync().name;
  const filename = Date.now().toString();
  const expected = path.join(destination, `${filename}.cbz`);

  await create({
    source,
    destination,
    filename,
  });

  const files = await fs.readdir(destination);

  t.is(files.length, 1);
  t.is(files[0], `${filename}.cbz`);

  const stat = await fs.stat(expected);
  t.true(stat.isFile());
  t.true(stat.size > 0);
});
