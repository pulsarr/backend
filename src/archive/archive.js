const { createWriteStream } = require('fs');
const fs = require('fs').promises;
const path = require('path');
const archiver = require('archiver');

/**
 * @async
 * @param {object} parameters function parameters
 * @param {string} parameters.source source of all images to be archived
 * @param {string} parameters.destination folder path for output file
 * @param {string} parameters.filename filename of output file with no extension
 * @returns {Promise<void>} finalized promise of the archive
 */
async function create({ source, destination, filename }) {
  const archivePath = path.join(destination, `${filename}.zip`);

  const output = createWriteStream(archivePath);

  const archive = archiver('zip', { zlib: { level: 9 } });
  archive.pipe(output);

  output.on('close', () => {
    console.debug('close');
  });

  output.on('end', (error) => {
    console.debug(error);
  });

  archive.on('warning', (error) => {
    console.error(error);
  });

  archive.on('error', (error) => {
    console.error(error);
  });

  archive.glob('./**/*', {
    cwd: source,
  });
  await archive.finalize();

  return fs.rename(archivePath, path.join(destination, `${filename}.cbz`));
}

module.exports = {
  create,
};
