const { parse, toDate } = require('date-fns');

const seriesListURL = 'https://www.mangareader.net/alphabetical';

/**
 * @async
 * @returns {Promise} returns this source (not from the db)
 */
async function getSource() {
  return {
    name: 'Manga Reader',
    url: seriesListURL,
  };
}

/**
 * @async
 * @param {Function} page page object from puppeteer
 * @returns {Promise} all series scraped metadata
 */
async function getAllSeries(page) {
  await page.goto(seriesListURL);

  return page.$$eval('.series_col li', (results) =>
    results.map((r) => ({
      url: r.querySelector('a').href,
      isCompleted: r.querySelector('.mangacompleted') !== null,
    }))
  );
}

/**
 * @async
 * @param {Function} page page object from puppeteer
 * @param {string} seriesUrl url for the series
 * @returns {Promise} all chapters for the given series
 */
async function getAllChapters(page, seriesUrl) {
  await page.goto(seriesUrl);

  const chapters = await page.$$eval(
    '#listing tr:not(:first-child)',
    (results) =>
      results.map((r) => ({
        url: r.querySelector('a').href,
        title: r.querySelector('a').textContent.trim(),
        dateReleased: r.querySelector('td:not(:first-child)').textContent,
      }))
  );

  return chapters.map((c) => {
    // eslint-disable-next-line no-param-reassign
    c.dateReleased = toDate(parse(c.dateReleased, 'MM/dd/yyyy', new Date()));
    return c;
  });
}

/**
 * @async
 * @param {Function} page page object from puppeteer
 * @param {string} chapterUrl url for a chapter
 * @returns {Promise} all pages for a given chapter
 */
async function getAllPages(page, chapterUrl) {
  await page.goto(chapterUrl);

  return page.$$eval('#pageMenu option', (results) =>
    results.map((r) => ({
      url: `${window.location.origin}${r.value}`,
    }))
  );
}

/**
 * @async
 * @param {Function} page page object from puppeteer
 * @param {string} pageUrl url for a page
 * @returns {Promise} the image url that can be downloaded and read
 */
async function getImage(page, pageUrl) {
  await page.goto(pageUrl);

  return page.$eval('#img', (i) => ({
    url: i.src,
  }));
}

module.exports = {
  getSource,
  getAllSeries,
  getAllChapters,
  getAllPages,
  getImage,
};
