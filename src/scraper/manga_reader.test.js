const test = require('ava');
const withPage = require('../../test/_withPage');

const mr = require('./manga_reader');

const expected = {
  url: 'http://www.mangareader.net/ichiba-kurogane-wa-kasegitai',
  title: 'Ichiba Kurogane wa Kasegitai 1',
  chapterUrl: 'http://www.mangareader.net/ichiba-kurogane-wa-kasegitai/1',
  chapterDate: {
    day: 24,
    month: 5,
    year: 2018,
  },
  pageUrl: 'http://www.mangareader.net/ichiba-kurogane-wa-kasegitai/1',
  imageUrl:
    'https://i6.imggur.net/ichiba-kurogane-wa-kasegitai/1/ichiba-kurogane-wa-kasegitai-10790971.jpg',
};

test('int manga reader should be able to get source', async (t) => {
  const { name, url } = await mr.getSource();

  t.is(name, 'Manga Reader');
  t.is(url, 'https://www.mangareader.net/alphabetical');
});

test(
  'int manga reader should be able to get all series',
  withPage,
  async (t, page) => {
    const series = await mr.getAllSeries(page);

    t.true(series.length > 0);

    const { isCompleted, url } = series[0];
    t.true(isCompleted);
    t.is(url, expected.url);
  }
);

test(
  'int manga reader should be able to get all chapters',
  withPage,
  async (t, page) => {
    const chapters = await mr.getAllChapters(page, expected.url);

    t.true(chapters.length > 0);

    const { dateReleased, title, url } = chapters[0];

    t.is(dateReleased.getDate(), expected.chapterDate.day);
    t.is(dateReleased.getMonth() + 1, expected.chapterDate.month);
    t.is(dateReleased.getFullYear(), expected.chapterDate.year);
    t.is(title, expected.title);
    t.is(url, expected.chapterUrl);
  }
);

test(
  'int manga reader should be able to get all pages',
  withPage,
  async (t, page) => {
    const pages = await mr.getAllPages(page, expected.chapterUrl);

    t.true(pages.length > 0);

    const { url } = pages[0];

    t.is(url, expected.pageUrl);
  }
);

test(
  'int manga reader should be able to get all single page',
  withPage,
  async (t, page) => {
    const { url } = await mr.getImage(page, expected.pageUrl);

    t.is(url, expected.imageUrl);
  }
);
