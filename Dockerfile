FROM node:14.3

# puppeteer
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN  apt-get update \
     && apt-get install --no-install-recommends -y wget gnupg ca-certificates \
     && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
     && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
     && apt-get update \
     && apt-get install --no-install-recommends -y google-chrome-stable \
     && rm -rf /var/lib/apt/lists/* \
     && wget --quiet https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh -O /usr/sbin/wait-for-it.sh \
     && chmod +x /usr/sbin/wait-for-it.sh

WORKDIR /app

COPY package* ./
RUN npm install

COPY . ./

EXPOSE 3000
ENTRYPOINT [ "npm" ]
CMD ["run", "serve"]
