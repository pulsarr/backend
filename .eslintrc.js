module.exports = {
  env: {
    commonjs: true,
    es6: true,
    node: true,
  },
  extends: [
    'airbnb-base',
    'plugin:jsdoc/recommended',
    'plugin:prettier/recommended',
    'prettier',
    'plugin:ava/recommended',
    'plugin:node/recommended',
    'plugin:security/recommended',
    'plugin:unicorn/recommended',
  ],
  plugins: ['jsdoc', 'ava', 'node', 'security', 'unicorn'],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
    window: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 11,
  },
  rules: {
    quotes: ['error', 'single'],
    'prettier/prettier': 'error',
    'no-console': 'off',
    'security/detect-non-literal-fs-filename': 0,
    'security/detect-non-literal-require': 0,
    'security/detect-object-injection': 0,
    'node/no-unpublished-require': 0,
    'unicorn/filename-case': 0,
  },
};
